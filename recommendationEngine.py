import math
import numpy as np

def process():

    vectors = []


    with open("TFIDF.csv", "r") as file:
        stories = file.readlines()
        header = stories[0].replace("\n", "")

        vectors.append(header)
        toAdd = []

        #documents vector calculation
        for i in range(1,5):
            col = 0
            for row in stories[2:]:
                row = row.replace("[", "").replace("]", "").replace("\n", "").replace('\'', "")
                splitted = row.split(",")
                col += math.pow(float(splitted[i]),2)
            vector = math.sqrt(col)
            toAdd.append(vector)
        vectors.append(toAdd)

        #query vector calculation
        totalPresent = 0
        for row in stories[2:]:
            row = row.replace("[", "").replace("]", "").replace("\n", "").replace('\'', "")
            splitted = row.split(",")
            if float(splitted[5]) != float(0.0):
                totalPresent += 1
        totalPresent = math.sqrt(totalPresent)
        vectors[1].append(totalPresent)

        # document TFIDF * Query term freqeuncy = dot prod
        # sum van alle woorden = (dot prod)
        # vector length document * vector query = len*query
        # sum(dot prod) / (len*query) = cosine

    ranked =[vectors[0][5:],["",]]

    for i in range(0,4):
        d1 = []
        query = []
        temp = 0
        cosine = 0
        with open("TFIDF.csv", "r") as file:
            for line in file.readlines()[2:]:
                line = line.replace("[", "").replace("]", "").replace("\n", "").replace('\'', "")
                splitted = line.split(",")
                d1.append(splitted[i+1])
        with open("result.csv", "r") as file1: 
            for line1 in file1.readlines()[1:]:
                line1 = line1.replace("[", "").replace("]", "").replace("\n", "").replace('\'', "")
                splitted1 = line1.split(",")
                query.append(splitted1[i+1])
        for k in range(0,len(d1)):
            tempdot = float(d1[k]) * float(query[k])
            temp += tempdot
        
        lenQuery = vectors[1][i] * vectors [1][4]
        cosine = temp / lenQuery
        ranked[1].append(cosine)
        print(ranked)
        np.savetxt("ranked.csv", ranked, delimiter = ",", fmt = '% s')

        



        
        
