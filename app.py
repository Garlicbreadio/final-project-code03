from flask import Flask, render_template, request, redirect, flash
from werkzeug.utils import secure_filename
import pandas
import os
import csv
import pre_proccessing_module
import recommendationEngine

UPLOADS = os.path.dirname(os.path.abspath(__file__)) + '/development_material/stories_collection/'
ALLOWED_EXTENSIONS = {'txt'}

app = Flask(__name__, static_url_path="/static")
DIR_PATH = os.path.dirname(os.path.realpath(__file__))
app.config['UPLOADS'] = UPLOADS
app.secret_key = '123'

#checks if the extension of the uploaded file is permitted
def allowed_file(filename):
    return '.' in filename and os.path.splitext(filename)[1][1:] in ALLOWED_EXTENSIONS

#our contactpage
@app.route("/contact", methods=["GET",'POST'])
def contactpage():
	return render_template("contact.html")

#handles uploaded file from html and saves them in /development_material/stories_collection/
@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        try:
            with open('ranked.csv', 'r') as csv_file:
                data = csv_file.readlines()
            
    
            resultData = data[1].replace("]","").split(",")[1:]
            headers = data[0].replace("]","").split(",")
            headers.remove("query.txt")
            return render_template('index.html', resultData=resultData, headers=headers)
        except:
            return render_template("index.html")
    if request.method == 'POST':
        file = request.files['file']
        if file.filename == '':
            flash('You forgot to select a file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            #returns a secure version of the file using werkzeug.utils library so it can be safely stored and passed onto os.path.join 
            file.save(os.path.join(app.config['UPLOADS'], "query.txt"))
            #renames the file so it will be at the end of the list for the preprocessing module
            pre_proccessing_module.process()
            recommendationEngine.process() 
        with open('ranked.csv', 'r') as csv_file:
            data = csv_file.readlines()
        
    
    resultData = data[1].replace("]","").split(",")[1:]
    headers = data[0].replace("]","").split(",")
    headers.remove("query.txt")
    return render_template('index.html', resultData=resultData, headers=headers)

@app.route("/contact", methods=["GET",'POST'])
def results():

	return render_template("results.html")

#@app.route('/development_material/stories_collection/<filename>')
#def results():
#    return render_template('results.html')

if __name__ == "__main__":
	app.config['TEMPLATES_AUTO_RELOAD']=True
	app.config['DEBUG'] = True
	app.config['SERVER_NAME'] = "127.0.0.1:5000"         
	app.run(debug=True)