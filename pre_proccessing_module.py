#pre-proccessing module
import re
import os
import csv
import math
import numpy as np 

def process():
    #variables
    wordcounts = {}
    file1 = {}
    files = os.listdir("development_material/stories_collection")

    #filtering document punctuation, stopwords and convert to lowercase
    for filename in files:
        with open("development_material/stories_collection/{}".format(filename), "r") as file:
            #text punctuation filter
            text = re.sub('[^\w\s]','',"".join(file.readlines()))
            text = text.replace("\n", " ")
            text = text.lower()
            text = text.split(" ")
            #filter for stopwords, change dutch to english if language changed
            with open("development_material/stop_words/dutch")as stopwords:
                words = stopwords.readlines()
                for word in words:
                    word = word.replace("\n","")
                    for textword in text:
                        if len(textword) < 1:
                            text.remove(textword)
                        if word == textword:
                            text.remove(textword)

            for freq in text:
                file1[freq] = text.count(freq)
        wordcounts[filename] = file1.copy()
        file1.clear()

    #variables
    headers = ["term"]
    words = {}
    to_add = []

    #data switcheroo to prepare for matrix
    for header in range(0,len(wordcounts.keys())):
        current_header = list(wordcounts)[header]
        headers.append(current_header)
        for word in wordcounts[current_header]:
            if word not in words.keys(): 
                words[word] = [0,0,0,0,0]
            words[word][header] = wordcounts[current_header][word]

    #add words to dict
    for word in words.keys():
        current_word = word
        to_add.append([current_word, words[current_word][0],words[current_word][1],words[current_word][2],words[current_word][3],words[current_word][4]])

    #write to csv file named "result.csv"
    with open("result.csv", 'w',newline = "") as result:
        writer = csv.writer(result)
        writer.writerow(headers)
        writer.writerows(to_add)

    TFIDF = []

    with open("result.csv", "r") as file:
        totalDocs = 5
        result = file.readlines()
        #add header to TFIDF
        TFIDF.append(result[0])
        #writer.writerows(result[0])
        result = result[1:]
        for row in result:
            row = row.replace("[", "").replace("]", "").replace("\n", "").replace('\'', "")
            print(row)
            splitted = row.split(",")
            print(splitted)
            toAppend = [splitted[0]]
            #the formula 
            c = 0
            for l in range(1,6):
                if float(splitted[l]) != 0.0:
                    c += 1
            log = math.log2(totalDocs/c)
            for i in range(1,len(splitted)):
                if str(splitted[i]) == "0":
                    toAppend.append(0)
                    continue
                else:
                    toAppend.append(log)
            #write to csv file named "TFIDF.csv"
            TFIDF.append(toAppend)
    np.savetxt("TFIDF.csv", TFIDF, delimiter = ",", fmt = '% s')